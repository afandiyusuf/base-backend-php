<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<div class="text-center" style="padding:50px 100px">
	<div class="logo">login</div>
        <div class="login-form-1">
        
                <form id="login-form" class="text-left" action="<?= $this->Url->build(["controller" => "login", "action" => "process"]) ?>">
                    <div class="login-form-main-message"></div>
                    <div class="main-login-form">
                        <div class="login-group">
                            <div class="form-group">
                                <label for="lg_username" class="sr-only">Username</label>
                                <input type="text" class="form-control" id="lg_username" name="lg_username" placeholder="username">
                            </div>
                            <div class="form-group">
                                <label for="lg_password" class="sr-only">Password</label>
                                <input type="password" class="form-control" id="lg_password" name="lg_password" placeholder="password">
                            </div>
                            <div class="form-group login-group-checkbox">
                                <input type="checkbox" id="lg_remember" name="lg_remember">
                                <label for="lg_remember">remember</label>
                            </div>
                        </div>
                        <button type="submit" class="login-button">LOGIN</button>
                    </div>
                </form>
            </div>
        </div
    </div>
</div>