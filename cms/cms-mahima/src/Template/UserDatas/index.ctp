<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\UserData[]|\Cake\Collection\CollectionInterface $userDatas
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Data'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userDatas index large-9 medium-8 columns content">
    <h3><?= __('User Datas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prop') ?></th>
                <th scope="col"><?= $this->Paginator->sort('val') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_cred_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userDatas as $userData): ?>
            <tr>
                <td><?= $this->Number->format($userData->id) ?></td>
                <td><?= h($userData->prop) ?></td>
                <td><?= h($userData->val) ?></td>
                <td><?= $userData->has('user_cred') ? $this->Html->link($userData->user_cred->id, ['controller' => 'UserCreds', 'action' => 'view', $userData->user_cred->id]) : '' ?></td>
                <td><?= h($userData->created_at) ?></td>
                <td><?= h($userData->updated_at) ?></td>
                <td><?= h($userData->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userData->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userData->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userData->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userData->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
