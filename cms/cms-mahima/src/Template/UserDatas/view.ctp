<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\UserData $userData
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Data'), ['action' => 'edit', $userData->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Data'), ['action' => 'delete', $userData->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userData->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Datas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Data'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userDatas view large-9 medium-8 columns content">
    <h3><?= h($userData->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Prop') ?></th>
            <td><?= h($userData->prop) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Val') ?></th>
            <td><?= h($userData->val) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Cred') ?></th>
            <td><?= $userData->has('user_cred') ? $this->Html->link($userData->user_cred->id, ['controller' => 'UserCreds', 'action' => 'view', $userData->user_cred->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userData->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($userData->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($userData->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($userData->deleted_at) ?></td>
        </tr>
    </table>
</div>
