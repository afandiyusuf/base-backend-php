<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\UserCred[]|\Cake\Collection\CollectionInterface $userCreds
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Cred'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Access Tokens'), ['controller' => 'AccessTokens', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Access Token'), ['controller' => 'AccessTokens', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Claimeds'), ['controller' => 'Claimeds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Claimed'), ['controller' => 'Claimeds', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Datas'), ['controller' => 'UserDatas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Data'), ['controller' => 'UserDatas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userCreds index large-9 medium-8 columns content">
    <h3><?= __('User Creds') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col">total_tiket</th>
                <th scope="col">total_coin</th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userCreds as $userCred): ?>
            <tr>
                <td><?= $this->Number->format($userCred->id) ?></td>
                <td><?= h($userCred->username) ?></td>
                <td><?= h($userCred->email) ?></td>
                <td><?= h( $userCred->virtualProperties(['total_tiket'] )->total_tiket) ?></td>
                <td><?= h( $userCred->virtualProperties(['total_coin'] )->total_coin) ?></td>
                <td><?= h($userCred->created_at) ?></td>
                <td><?= h($userCred->updated_at) ?></td>
                
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userCred->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userCred->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userCred->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userCred->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
