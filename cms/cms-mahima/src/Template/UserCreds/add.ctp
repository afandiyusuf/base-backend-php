<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List User Creds'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Access Tokens'), ['controller' => 'AccessTokens', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Access Token'), ['controller' => 'AccessTokens', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Claimeds'), ['controller' => 'Claimeds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Claimed'), ['controller' => 'Claimeds', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Datas'), ['controller' => 'UserDatas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Data'), ['controller' => 'UserDatas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userCreds form large-9 medium-8 columns content">
    <?= $this->Form->create($userCred) ?>
    <fieldset>
        <legend><?= __('Add User Cred') ?></legend>
        <?php
            echo $this->Form->control('username');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('created_at');
            echo $this->Form->control('updated_at');
            echo $this->Form->control('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
