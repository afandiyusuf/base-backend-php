<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\UserCred $userCred
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Cred'), ['action' => 'edit', $userCred->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Cred'), ['action' => 'delete', $userCred->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userCred->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Creds'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Cred'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Access Tokens'), ['controller' => 'AccessTokens', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Access Token'), ['controller' => 'AccessTokens', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Claimeds'), ['controller' => 'Claimeds', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Claimed'), ['controller' => 'Claimeds', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Slots'), ['controller' => 'Slots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Slot'), ['controller' => 'Slots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Datas'), ['controller' => 'UserDatas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Data'), ['controller' => 'UserDatas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userCreds view large-9 medium-8 columns content">
    <h3><?= h($userCred->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($userCred->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($userCred->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($userCred->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userCred->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($userCred->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($userCred->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($userCred->deleted_at) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Access Tokens') ?></h4>
        <?php if (!empty($userCred->access_tokens)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Secret Token') ?></th>
                <th scope="col"><?= __('User Cred Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userCred->access_tokens as $accessTokens): ?>
            <tr>
                <td><?= h($accessTokens->id) ?></td>
                <td><?= h($accessTokens->secret_token) ?></td>
                <td><?= h($accessTokens->user_cred_id) ?></td>
                <td><?= h($accessTokens->created_at) ?></td>
                <td><?= h($accessTokens->updated_at) ?></td>
                <td><?= h($accessTokens->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'AccessTokens', 'action' => 'view', $accessTokens->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'AccessTokens', 'action' => 'edit', $accessTokens->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'AccessTokens', 'action' => 'delete', $accessTokens->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accessTokens->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Claimeds') ?></h4>
        <?php if (!empty($userCred->claimeds)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Qrcodes Id') ?></th>
                <th scope="col"><?= __('User Cred Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userCred->claimeds as $claimeds): ?>
            <tr>
                <td><?= h($claimeds->id) ?></td>
                <td><?= h($claimeds->qrcodes_id) ?></td>
                <td><?= h($claimeds->user_cred_id) ?></td>
                <td><?= h($claimeds->created_at) ?></td>
                <td><?= h($claimeds->updated_at) ?></td>
                <td><?= h($claimeds->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Claimeds', 'action' => 'view', $claimeds->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Claimeds', 'action' => 'edit', $claimeds->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Claimeds', 'action' => 'delete', $claimeds->id], ['confirm' => __('Are you sure you want to delete # {0}?', $claimeds->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Slots') ?></h4>
        <?php if (!empty($userCred->slots)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Cred Id') ?></th>
                <th scope="col"><?= __('Tiket Val') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userCred->slots as $slots): ?>
            <tr>
                <td><?= h($slots->id) ?></td>
                <td><?= h($slots->user_cred_id) ?></td>
                <td><?= h($slots->tiket_val) ?></td>
                <td><?= h($slots->created_at) ?></td>
                <td><?= h($slots->updated_at) ?></td>
                <td><?= h($slots->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Slots', 'action' => 'view', $slots->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Slots', 'action' => 'edit', $slots->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Slots', 'action' => 'delete', $slots->id], ['confirm' => __('Are you sure you want to delete # {0}?', $slots->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Datas') ?></h4>
        <?php if (!empty($userCred->user_datas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Prop') ?></th>
                <th scope="col"><?= __('Val') ?></th>
                <th scope="col"><?= __('User Cred Id') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col"><?= __('Updated At') ?></th>
                <th scope="col"><?= __('Deleted At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userCred->user_datas as $userDatas): ?>
            <tr>
                <td><?= h($userDatas->id) ?></td>
                <td><?= h($userDatas->prop) ?></td>
                <td><?= h($userDatas->val) ?></td>
                <td><?= h($userDatas->user_cred_id) ?></td>
                <td><?= h($userDatas->created_at) ?></td>
                <td><?= h($userDatas->updated_at) ?></td>
                <td><?= h($userDatas->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserDatas', 'action' => 'view', $userDatas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserDatas', 'action' => 'edit', $userDatas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserDatas', 'action' => 'delete', $userDatas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userDatas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
