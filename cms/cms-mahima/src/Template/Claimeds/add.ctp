<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Claimeds'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Qrcodes'), ['controller' => 'Qrcodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Qrcode'), ['controller' => 'Qrcodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="claimeds form large-9 medium-8 columns content">
    <?= $this->Form->create($claimed) ?>
    <fieldset>
        <legend><?= __('Add Claimed') ?></legend>
        <?php
            echo $this->Form->control('qrcodes_id', ['options' => $qrcodes]);
            echo $this->Form->control('user_cred_id', ['options' => $userCreds]);
            echo $this->Form->control('created_at');
            echo $this->Form->control('updated_at');
            echo $this->Form->control('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
