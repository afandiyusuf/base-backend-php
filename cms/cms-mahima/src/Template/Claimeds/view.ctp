<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Claimed $claimed
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Claimed'), ['action' => 'edit', $claimed->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Claimed'), ['action' => 'delete', $claimed->id], ['confirm' => __('Are you sure you want to delete # {0}?', $claimed->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Claimeds'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Claimed'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Qrcodes'), ['controller' => 'Qrcodes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Qrcode'), ['controller' => 'Qrcodes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="claimeds view large-9 medium-8 columns content">
    <h3><?= h($claimed->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Qrcode') ?></th>
            <td><?= $claimed->has('qrcode') ? $this->Html->link($claimed->qrcode->id, ['controller' => 'Qrcodes', 'action' => 'view', $claimed->qrcode->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Cred') ?></th>
            <td><?= $claimed->has('user_cred') ? $this->Html->link($claimed->user_cred->id, ['controller' => 'UserCreds', 'action' => 'view', $claimed->user_cred->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($claimed->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($claimed->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($claimed->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($claimed->deleted_at) ?></td>
        </tr>
    </table>
</div>
