<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Claimed[]|\Cake\Collection\CollectionInterface $claimeds
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Claimed'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Qrcodes'), ['controller' => 'Qrcodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Qrcode'), ['controller' => 'Qrcodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="claimeds index large-9 medium-8 columns content">
    <h3><?= __('Claimeds') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('qrcodes_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_cred_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted_at') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($claimeds as $claimed): ?>
            <tr>
                <td><?= $this->Number->format($claimed->id) ?></td>
                <td><?= $claimed->has('qrcode') ? $this->Html->link($claimed->qrcode->id, ['controller' => 'Qrcodes', 'action' => 'view', $claimed->qrcode->id]) : '' ?></td>
                <td><?= $claimed->has('user_cred') ? $this->Html->link($claimed->user_cred->id, ['controller' => 'UserCreds', 'action' => 'view', $claimed->user_cred->id]) : '' ?></td>
                <td><?= h($claimed->created_at) ?></td>
                <td><?= h($claimed->updated_at) ?></td>
                <td><?= h($claimed->deleted_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $claimed->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $claimed->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $claimed->id], ['confirm' => __('Are you sure you want to delete # {0}?', $claimed->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
