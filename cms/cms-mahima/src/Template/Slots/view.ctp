<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Slot $slot
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Slot'), ['action' => 'edit', $slot->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Slot'), ['action' => 'delete', $slot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $slot->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Slots'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Slot'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="slots view large-9 medium-8 columns content">
    <h3><?= h($slot->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Cred') ?></th>
            <td><?= $slot->has('user_cred') ? $this->Html->link($slot->user_cred->id, ['controller' => 'UserCreds', 'action' => 'view', $slot->user_cred->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($slot->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tiket Val') ?></th>
            <td><?= $this->Number->format($slot->tiket_val) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($slot->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($slot->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($slot->deleted_at) ?></td>
        </tr>
    </table>
</div>
