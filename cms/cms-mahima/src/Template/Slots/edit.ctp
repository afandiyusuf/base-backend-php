<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $slot->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $slot->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Slots'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="slots form large-9 medium-8 columns content">
    <?= $this->Form->create($slot) ?>
    <fieldset>
        <legend><?= __('Edit Slot') ?></legend>
        <?php
            echo $this->Form->control('user_cred_id', ['options' => $userCreds]);
            echo $this->Form->control('tiket_val');
            echo $this->Form->control('created_at');
            echo $this->Form->control('updated_at');
            echo $this->Form->control('deleted_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
