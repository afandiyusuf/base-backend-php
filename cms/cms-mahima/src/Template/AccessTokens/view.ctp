<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\AccessToken $accessToken
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Access Token'), ['action' => 'edit', $accessToken->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Access Token'), ['action' => 'delete', $accessToken->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accessToken->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Access Tokens'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Access Token'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Creds'), ['controller' => 'UserCreds', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Cred'), ['controller' => 'UserCreds', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="accessTokens view large-9 medium-8 columns content">
    <h3><?= h($accessToken->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Secret Token') ?></th>
            <td><?= h($accessToken->secret_token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Cred') ?></th>
            <td><?= $accessToken->has('user_cred') ? $this->Html->link($accessToken->user_cred->id, ['controller' => 'UserCreds', 'action' => 'view', $accessToken->user_cred->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($accessToken->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($accessToken->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($accessToken->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted At') ?></th>
            <td><?= h($accessToken->deleted_at) ?></td>
        </tr>
    </table>
</div>
