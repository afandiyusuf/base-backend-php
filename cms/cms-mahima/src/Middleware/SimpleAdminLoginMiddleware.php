<?php
namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * SimpleAdminLogin middleware
 */
class SimpleAdminLoginMiddleware
{

    /**
     * Invoke method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Message\ResponseInterface $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return \Psr\Http\Message\ResponseInterface A response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        $path = $request->getUri()->getPath();
        
        $session = $request->session();
        $login_session = $session->read('login');
        if($path != "/login" && $path !="/login/process")
        {
            if($login_session == "true"){
                return $next($request, $response);
            }else{
                return new RedirectResponse("login");
            }
        }else if($login_session == "true")
        {
            return $next($request, $response);
        }

        return $next($request, $response);
        
    }
}
