<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserDatas Controller
 *
 * @property \App\Model\Table\UserDatasTable $UserDatas
 *
 * @method \App\Model\Entity\UserData[] paginate($object = null, array $settings = [])
 */
class UserDatasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserCreds']
        ];
        $userDatas = $this->paginate($this->UserDatas);

        $this->set(compact('userDatas'));
        $this->set('_serialize', ['userDatas']);
    }

    /**
     * View method
     *
     * @param string|null $id User Data id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userData = $this->UserDatas->get($id, [
            'contain' => ['UserCreds']
        ]);

        $this->set('userData', $userData);
        $this->set('_serialize', ['userData']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userData = $this->UserDatas->newEntity();
        if ($this->request->is('post')) {
            $userData = $this->UserDatas->patchEntity($userData, $this->request->getData());
            if ($this->UserDatas->save($userData)) {
                $this->Flash->success(__('The user data has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user data could not be saved. Please, try again.'));
        }
        $userCreds = $this->UserDatas->UserCreds->find('list', ['limit' => 200]);
        $this->set(compact('userData', 'userCreds'));
        $this->set('_serialize', ['userData']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Data id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userData = $this->UserDatas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userData = $this->UserDatas->patchEntity($userData, $this->request->getData());
            if ($this->UserDatas->save($userData)) {
                $this->Flash->success(__('The user data has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user data could not be saved. Please, try again.'));
        }
        $userCreds = $this->UserDatas->UserCreds->find('list', ['limit' => 200]);
        $this->set(compact('userData', 'userCreds'));
        $this->set('_serialize', ['userData']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Data id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userData = $this->UserDatas->get($id);
        if ($this->UserDatas->delete($userData)) {
            $this->Flash->success(__('The user data has been deleted.'));
        } else {
            $this->Flash->error(__('The user data could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
