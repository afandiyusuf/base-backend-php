<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Slots Controller
 *
 * @property \App\Model\Table\SlotsTable $Slots
 *
 * @method \App\Model\Entity\Slot[] paginate($object = null, array $settings = [])
 */
class SlotsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserCreds']
        ];
        $slots = $this->paginate($this->Slots);

        $this->set(compact('slots'));
        $this->set('_serialize', ['slots']);
    }

    /**
     * View method
     *
     * @param string|null $id Slot id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $slot = $this->Slots->get($id, [
            'contain' => ['UserCreds']
        ]);

        $this->set('slot', $slot);
        $this->set('_serialize', ['slot']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $slot = $this->Slots->newEntity();
        if ($this->request->is('post')) {
            $slot = $this->Slots->patchEntity($slot, $this->request->getData());
            if ($this->Slots->save($slot)) {
                $this->Flash->success(__('The slot has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The slot could not be saved. Please, try again.'));
        }
        $userCreds = $this->Slots->UserCreds->find('list', ['limit' => 200]);
        $this->set(compact('slot', 'userCreds'));
        $this->set('_serialize', ['slot']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Slot id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $slot = $this->Slots->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $slot = $this->Slots->patchEntity($slot, $this->request->getData());
            if ($this->Slots->save($slot)) {
                $this->Flash->success(__('The slot has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The slot could not be saved. Please, try again.'));
        }
        $userCreds = $this->Slots->UserCreds->find('list', ['limit' => 200]);
        $this->set(compact('slot', 'userCreds'));
        $this->set('_serialize', ['slot']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Slot id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $slot = $this->Slots->get($id);
        if ($this->Slots->delete($slot)) {
            $this->Flash->success(__('The slot has been deleted.'));
        } else {
            $this->Flash->error(__('The slot could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
