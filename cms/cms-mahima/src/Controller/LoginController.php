<?php
namespace App\Controller;

use App\Controller\AppController;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Login Controller
 *
 *
 * @method \App\Model\Entity\Login[] paginate($object = null, array $settings = [])
 */
class LoginController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // $this->set(compact('login'));
        // $this->set('_serialize', ['login']);
    }

    public function process()
    {
        if($_GET['lg_username'] == "admin" && $_GET['lg_password']=="mahiMA123qwe")
        {
            $session = $this->request->session();
            $session->write('login', 'true');
            return $this->redirect(
                ['controller' => 'UserCreds', 'action' => 'index']
            );
            
        }else{
            $session = $this->request->session();
            $session->write('login', 'false');
            return $this->redirect(
                ['controller' => 'Login', 'action' => 'index']
            );
        }
    }
    public function logout()
    {
        $session = $this->request->session();
        $session->write('login','false');
        return $this->redirect(
            [
                'controller' => 'UserCreds','action'=>'index'
            ]
        );
    }
}
