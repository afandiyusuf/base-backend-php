<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserCreds Controller
 *
 * @property \App\Model\Table\UserCredsTable $UserCreds
 *
 * @method \App\Model\Entity\UserCred[] paginate($object = null, array $settings = [])
 */
class UserCredsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $userCreds = $this->paginate($this->UserCreds);

        $this->set(compact('userCreds'));
        $this->set('_serialize', ['userCreds']);
    }

    /**
     * View method
     *
     * @param string|null $id User Cred id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userCred = $this->UserCreds->get($id, [
            'contain' => ['AccessTokens', 'Claimeds', 'Slots', 'UserDatas']
        ]);

        $this->set('userCred', $userCred);
        $this->set('_serialize', ['userCred']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userCred = $this->UserCreds->newEntity();
        if ($this->request->is('post')) {
            $userCred = $this->UserCreds->patchEntity($userCred, $this->request->getData());
            if ($this->UserCreds->save($userCred)) {
                $this->Flash->success(__('The user cred has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user cred could not be saved. Please, try again.'));
        }
        $this->set(compact('userCred'));
        $this->set('_serialize', ['userCred']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Cred id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userCred = $this->UserCreds->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userCred = $this->UserCreds->patchEntity($userCred, $this->request->getData());
            if ($this->UserCreds->save($userCred)) {
                $this->Flash->success(__('The user cred has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user cred could not be saved. Please, try again.'));
        }
        $this->set(compact('userCred'));
        $this->set('_serialize', ['userCred']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Cred id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userCred = $this->UserCreds->get($id);
        if ($this->UserCreds->delete($userCred)) {
            $this->Flash->success(__('The user cred has been deleted.'));
        } else {
            $this->Flash->error(__('The user cred could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
