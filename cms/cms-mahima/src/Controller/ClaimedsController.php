<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Claimeds Controller
 *
 * @property \App\Model\Table\ClaimedsTable $Claimeds
 *
 * @method \App\Model\Entity\Claimed[] paginate($object = null, array $settings = [])
 */
class ClaimedsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Qrcodes', 'UserCreds']
        ];
        $claimeds = $this->paginate($this->Claimeds);

        $this->set(compact('claimeds'));
        $this->set('_serialize', ['claimeds']);
    }

    /**
     * View method
     *
     * @param string|null $id Claimed id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $claimed = $this->Claimeds->get($id, [
            'contain' => ['Qrcodes', 'UserCreds']
        ]);

        $this->set('claimed', $claimed);
        $this->set('_serialize', ['claimed']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $claimed = $this->Claimeds->newEntity();
        if ($this->request->is('post')) {
            $claimed = $this->Claimeds->patchEntity($claimed, $this->request->getData());
            if ($this->Claimeds->save($claimed)) {
                $this->Flash->success(__('The claimed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The claimed could not be saved. Please, try again.'));
        }
        $qrcodes = $this->Claimeds->Qrcodes->find('list', ['limit' => 200]);
        $userCreds = $this->Claimeds->UserCreds->find('list', ['limit' => 200]);
        $this->set(compact('claimed', 'qrcodes', 'userCreds'));
        $this->set('_serialize', ['claimed']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Claimed id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $claimed = $this->Claimeds->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $claimed = $this->Claimeds->patchEntity($claimed, $this->request->getData());
            if ($this->Claimeds->save($claimed)) {
                $this->Flash->success(__('The claimed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The claimed could not be saved. Please, try again.'));
        }
        $qrcodes = $this->Claimeds->Qrcodes->find('list', ['limit' => 200]);
        $userCreds = $this->Claimeds->UserCreds->find('list', ['limit' => 200]);
        $this->set(compact('claimed', 'qrcodes', 'userCreds'));
        $this->set('_serialize', ['claimed']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Claimed id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $claimed = $this->Claimeds->get($id);
        if ($this->Claimeds->delete($claimed)) {
            $this->Flash->success(__('The claimed has been deleted.'));
        } else {
            $this->Flash->error(__('The claimed could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
