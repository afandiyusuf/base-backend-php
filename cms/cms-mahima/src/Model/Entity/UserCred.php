<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * UserCred Entity
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property \Cake\I18n\FrozenTime $deleted_at
 *
 * @property \App\Model\Entity\AccessToken[] $access_tokens
 * @property \App\Model\Entity\Claimed[] $claimeds
 * @property \App\Model\Entity\Slot[] $slots
 * @property \App\Model\Entity\UserData[] $user_datas
 */
class UserCred extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _getTotalTiket()
    {
        $slots = TableRegistry::get('slots');
        $query = $slots->find();
        $query->where(['user_cred_id'=>$this->_properties['id']]);
        $grandTotal = 0;
        foreach($query as $row)
        {
            $grandTotal += $row->tiket_val;
        }
        return $grandTotal;
    }

    protected function _getTotalCoin()
    {
        $slots = TableRegistry::get('claimeds');
        $query = $slots->find();
        $query->where(['user_cred_id'=>$this->_properties['id']]);
        $grandTotal = count($query);
        return $grandTotal;
    }
}
