<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserCreds Model
 *
 * @property \App\Model\Table\AccessTokensTable|\Cake\ORM\Association\HasMany $AccessTokens
 * @property \App\Model\Table\ClaimedsTable|\Cake\ORM\Association\HasMany $Claimeds
 * @property \App\Model\Table\SlotsTable|\Cake\ORM\Association\HasMany $Slots
 * @property \App\Model\Table\UserDatasTable|\Cake\ORM\Association\HasMany $UserDatas
 *
 * @method \App\Model\Entity\UserCred get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserCred newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserCred[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserCred|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserCred patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserCred[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserCred findOrCreate($search, callable $callback = null, $options = [])
 */
class UserCredsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_creds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('AccessTokens', [
            'foreignKey' => 'user_cred_id'
        ]);
        $this->hasMany('Claimeds', [
            'foreignKey' => 'user_cred_id'
        ]);
        $this->hasMany('Slots', [
            'foreignKey' => 'user_cred_id'
        ]);
        $this->hasMany('UserDatas', [
            'foreignKey' => 'user_cred_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->dateTime('created_at')
            ->requirePresence('created_at', 'create')
            ->notEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }

   
}
