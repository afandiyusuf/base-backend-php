<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Slots Model
 *
 * @property \App\Model\Table\UserCredsTable|\Cake\ORM\Association\BelongsTo $UserCreds
 *
 * @method \App\Model\Entity\Slot get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slot newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slot[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slot|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slot patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slot[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slot findOrCreate($search, callable $callback = null, $options = [])
 */
class SlotsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('slots');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('UserCreds', [
            'foreignKey' => 'user_cred_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('tiket_val')
            ->requirePresence('tiket_val', 'create')
            ->notEmpty('tiket_val');

        $validator
            ->dateTime('created_at')
            ->requirePresence('created_at', 'create')
            ->notEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->requirePresence('updated_at', 'create')
            ->notEmpty('updated_at');

        $validator
            ->dateTime('deleted_at')
            ->allowEmpty('deleted_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_cred_id'], 'UserCreds'));

        return $rules;
    }
}
