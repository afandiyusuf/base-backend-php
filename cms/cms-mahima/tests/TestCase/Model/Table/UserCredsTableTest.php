<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserCredsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserCredsTable Test Case
 */
class UserCredsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserCredsTable
     */
    public $UserCreds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_creds',
        'app.access_tokens',
        'app.claimeds',
        'app.slots',
        'app.user_datas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserCreds') ? [] : ['className' => UserCredsTable::class];
        $this->UserCreds = TableRegistry::get('UserCreds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserCreds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
