<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimedsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimedsTable Test Case
 */
class ClaimedsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimedsTable
     */
    public $Claimeds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.claimeds',
        'app.qrcodes',
        'app.user_creds',
        'app.access_tokens',
        'app.slots',
        'app.user_datas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Claimeds') ? [] : ['className' => ClaimedsTable::class];
        $this->Claimeds = TableRegistry::get('Claimeds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Claimeds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
