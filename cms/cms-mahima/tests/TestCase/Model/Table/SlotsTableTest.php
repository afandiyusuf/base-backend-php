<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SlotsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SlotsTable Test Case
 */
class SlotsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SlotsTable
     */
    public $Slots;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.slots',
        'app.user_creds',
        'app.access_tokens',
        'app.claimeds',
        'app.qrcodes',
        'app.user_datas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Slots') ? [] : ['className' => SlotsTable::class];
        $this->Slots = TableRegistry::get('Slots', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Slots);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
