<?php

// Routes
$app->group('/api',function(){

	//api for auth
	$this->group('/auth',function(){
		$this->post('/login', AuthController::class.':login'); //login
		$this->post('/register', AuthController::class.':register');
		$this->post('/update_user_data', AuthController::class.':updateUserData')->add(new TokenChecker($this));
		$this->post('/get_meta', AuthController::class.':get_meta')->add(new TokenChecker($this));
		$this->post('/update_meta', AuthController::class.':update_meta')->add(new TokenChecker($this));
	});

	$this->group('/barcode',function(){
		$this->post('/check', BarcodeController::class.':check');//check barcode that can be claim or not
		$this->post('/claim', BarcodeController::class.':claim');//claim barcode
	})->add(new TokenChecker($this));

	$this->group('/slot',function(){
		$this->post('/list_slot', BarcodeController::class.':list_slot');//get list slot from server
		$this->post('/exchange', BarcodeController::class.':exchange'); //exchange 1 coin for slot
	})->add(new TokenChecker($this));


	$this->group('/prize',function(){
		
		$this->post('/get',PrizeController::class.':get_prizes');
		$this->post('/reedem_prize',PrizeController::class.':reedem_prize');
		$this->post('/get_history',PrizeController::class.':get_history');
	})->add(new TokenChecker($this));

	$this->group('/stores',function(){

		$this->post('/get',StoreController::class.':getAll');
		
	})->add(new TokenChecker($this));
	
	$this->post('/getBest10',AuthController::class.':getBest10');
});

