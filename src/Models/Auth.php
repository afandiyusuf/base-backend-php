<?php

use Medoo\Medoo;

class Auth{

	private $c;
	private $db;
	public static $ERROR = "error";
	public static $SUCCESS = "auth_success";
	public static $VALIDATE_ERROR = "validate_error";
	public static $SYSTEM_ERROR = "auth_system_error";
	public static $EMAIL_USERNAME_ERROR = "auth_username_email_error";
	public static $EMAIL_ERROR = "email_terpakai";
	public static $USERNAME_ERROR = "username_terpakai";
	private $secretSalt = "this is secret salt";


	function __construct($c){
		$this->db = $c->db;
	}

	public function register($username,$email,$password){
		$this->db->insert('user_creds',[
			'username' 	=> $username,
			'email'		=> $email,
			'password'	=> md5($username.$password.$this->secretSalt)
		]);

		return $this->db->id();
	}

	public function setAdditionalData($id,$additionalData)
	{
		for($i=0;$i<count($additionalData);$i++)
		{
			$total_addData = $this->db->count('user_datas',
				[
					'AND'=>[
						'prop' => $additionalData[$i]->name,
						'user_cred_id' => $id
					]
				]
			);
			if($total_addData == 1)
			{
				$this->db->update('user_datas',[
					'val' => $additionalData[$i]->val
				],[
					'AND' => [
						'prop' => $additionalData[$i]->name,
						'user_cred_id' => $id
					]
				]);
			}else{
				$this->db->insert('user_datas',[
					"prop" => $additionalData[$i]->name,
					"val" => $additionalData[$i]->val,
					"user_cred_id" => $id
				]);
			}
		}
	}

	public function loginWithUsername($username,$password){
		$total_user = $this->db->count('user_creds',[
			"AND"=>[
				'username' 	=> $username,
				'password'	=> md5($username.$password.$this->secretSalt)
				]
			]);

		if($total_user == 1)
		{
		 	$user_id = $this->db->get('user_creds','id',[
				"AND" =>[
					'username' 	=> $username,
					'password'	=> md5($username.$password.$this->secretSalt)
				]
			]);

			return $user_id;
		}else{
			return Auth::$ERROR;
		}
	}

	public function updatePassword($id,$password)
	{
		$username = $this->db->get('user_creds',["username"],["id"=>$id]);

		$this->db->update('user_creds',[
	    		'password' => md5($username["username"].$password.$this->secretSalt)
	    	],[
	    		'id'=>$id
	    	]);
	}

	public function loginWithEmail($email,$password){

	}

	public function validateAccessToken($id,$accessToken){
		$is_valid = $this->db->count('access_tokens',[
			"AND"=>[
				"secret_token"=>$accessToken,
				"user_cred_id" =>$id
			]			 
		]);

		if($is_valid == 1)
		{
			return Auth::$SUCCESS;
		}else{
			return Auth::$ERROR;
		}
	}

	public function checkUsernameEmail($username,$email){
		$total_user = $this->db->count('user_creds',[
				"username" => $username
		]);

		if($total_user > 0)
		{
			return Auth::$USERNAME_ERROR;
		}else{
			$total_user = $this->db->count('user_creds',[
					"email" => $email
			]);
			if($total_user > 0){
				return Auth::$EMAIL_ERROR;
			}else{
				return Auth::$SUCCESS;
			}
		}
	}

	public function validateAdditionalData($additionalData)
	{
		$Or_Query = [];
		for($i=0;$i<count($additionalData);$i++)
		{
			$OrData[] = $additionalData[$i]->name;
		}

		$total_data = $this->db->count('registered_datas',[
			"OR" => [
				"data_name" => $OrData
			]
		]);

		if($total_data == count($additionalData))
		{
			return Auth::$SUCCESS;
		}else{
			return Auth::$ERROR;
		}

	}

	public function generateAccessToken($user_id)
	{
		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $accessToken = '';
	    $length = 200;

	    for ($i = 0; $i < $length; $i++) {
	        $accessToken .= $characters[rand(0, $charactersLength - 1)];
	    }

	    $is_update = $this->db->count('access_tokens',[
	    	'user_cred_id' =>$user_id
	    ]);

	    if($is_update == 1)
	    {
	    	$this->db->update('access_tokens',[
	    		'secret_token' => $accessToken
	    	],[
	    		'user_cred_id'=>$user_id
	    	]);
	    }else{
			$this->db->insert('access_tokens',[
				'secret_token' 	=> $accessToken,
				'user_cred_id'	=> $user_id
			]);
	    }
	    return $accessToken;
	}

	public function getAdditionalData($id,$additionalData)
	{
		$data = [];
		
		for($i=0;$i<count($additionalData);$i++)
		{
			$data[] = $this->db->get('user_datas',[
				"prop",
				"val"
			],[
				"AND"=>[
					"prop" => $additionalData[$i]->name,
					"user_cred_id" => $id
				]
			]);

			if($data[$i] == false)
			{
				$this->db->insert('user_datas',[
					"prop"=>$additionalData[$i]->name,
					"val" => 0,
					"user_cred_id" => $id
				]);

				$data[$i] = $this->db->get('user_datas',[
					"prop",
					"val"
				],[
					"AND"=>[
						"prop" => $additionalData[$i]->name,
						"user_cred_id" => $id
					]
				]);
			}
		}



		return $data;
	}

	public function getTotalTiket($user_id)
	{
		return $this->db->sum('slots','tiket_val',['user_cred_id'=>$user_id]);

	}

	public function getLoginScreenUrl()
	{
		return $this->db->get('loadings',['thumbnail_url']);
	}

}
?>