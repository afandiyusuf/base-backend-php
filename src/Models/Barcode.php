<?php

use Medoo\Medoo;

class Barcode{
	private $c;
	private $db;

	public static $SUCCESS = "success";
	public static $NOT_FOUND = "not_found";
	public static $CLAIMED = "claimed";

	function __construct($c){
		$this->db = $c->db;
	}

	function checkBarcode($barcode){
		$barcode  = $this->db->get('qrcodes',"*",[
			"code"=>$barcode
		]);
		if(!$barcode)
		{
			return Barcode::$NOT_FOUND;
		}else{
			//check if qrcode is claimed
			$total_data = $this->db->count('claimeds',[
				"qrcodes_id"=>$barcode["id"]
			]);

			if($total_data == 1)
			{
				return Barcode::$CLAIMED;
			}else{
				return Barcode::$SUCCESS;
			}
		}
	}

	function claimBarcode($id,$barcode){
		$barcodeChecker = $this->checkBarcode($barcode);

		if($barcodeChecker == Barcode::$NOT_FOUND || $barcodeChecker == Barcode::$CLAIMED)
		{
			return $barcodeChecker;
		}else {
			$barcode  = $this->db->get('qrcodes',"*",[
				"code"=>$barcode
			]);

			$this->db->insert('claimeds',[
				"qrcodes_id"=>$barcode["id"],
				"user_cred_id"=>$id
			]);
		}
	}

	function logExcange($id,$total_tiket)
	{
		$this->db->insert('slots',[
			'user_cred_id'=>$id,
			'tiket_val'=>$total_tiket]);
	}

	function getBest10()
	{
		//$data = this->db->query("SELECT a.val as coin,a.prop as tipe,b.val as nama FROM user_data a LEFT JOIN user_data b ON a.user_cred_id = b.user_cred_id WHERE a.prop = 'tiket' AND b.prop = 'full_name' ORDER BY a.val ASC LIMIT 0,10")->fetchAll();
		
		$data = $this->db->query("SELECT SUM(slots.tiket_val) as coin , user_datas.val as nama FROM slots  LEFT JOIN user_datas ON user_datas.user_cred_id = slots.user_cred_id WHERE user_datas.prop = 'full_name' GROUP BY user_datas.user_cred_id ORDER BY coin DESC LIMIT 0,10")->fetchAll();

		
		return $data;
	}

	/**
	 * fungsi untuk mengambil list slot yang aktif
	 * @return array list slot
	 */
	public function get_list_slot()
	{
		$data = $this->db->select('management_slots',['name','description','thumbnail_url'],
			['is_active'=>1]);

		return $data;
	}

	/**
	 * fungsi untuk mengambil list slot yang aktif dan probabilitynya
	 * @return return list slot yang aktif beserta probabilitynya
	 */
	public function get_complete_list_slot()
	{
		$data = $this->db->select('management_slots',['name','description','thumbnail_url','probability','prize'],
		[
			'is_active'=>1,
			'ORDER' => ['probability'=>'ASC']
		]);

		return $data;
	}

	/**
	 * mendapatkan total probability dari hadiah yang tersedia
	 * @return integer, total probability dari semua hadiah
	 */
	public function get_all_prob()
	{
		$data = $this->db->sum('management_slots','probability',['is_active'=>1]);
		return $data;
	}
}

?>