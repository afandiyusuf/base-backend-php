<?php
use Medoo\Medoo;

class Store{

	private $c;
	private $db;

	function __construct($c){
		$this->db = $c->db;
	}

	public function get_total_store_every_province()
	{
		$db =$this->db;
		
		return $db->query(
			"
			SELECT p.name AS provinces, COUNT(s.name) AS total_store  FROM stores AS s LEFT JOIN provinces AS p ON s.province_id = p.id GROUP BY s.province_id
		")->fetchAll(\PDO::FETCH_ASSOC);
	}
	public function get_all_store()
	{
		$db = $this->db;

		$allData = $db->select("stores",[
			"[>]provinces"=>["stores.province_id"=>"id"]
		],[ 
			"provinces.name (province)",
			"stores.name (store_name)"
		],[
			"ORDER" => ["provinces.name" => "ASC"]
		]);
		return $allData;
	}

	public function get_store_by_province($id_province)
	{
		$db = $this->db;

		$allData = $db->select("stores",["name","address"],["province_id"=>$id_province]);

		return $allData;
	}

	public function get_all_provinces()
	{
		$db = $this->db;

		$allData = $db->select("stores",[
			"[>]provinces" => ["stores.province_id"=>"id"]
		],[
			"provinces.id (id)",
			"provinces.name (province)",

		],[
			"GROUP" => "provinces.id",
			"ORDER" => ["provinces.name" => "ASC"]

		]);

		return $allData;
	}
}

?>