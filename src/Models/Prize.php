<?php

use Medoo\Medoo;
use \Helper\ReturnData as ReturnData;

class Prize{
	private $c;
	private $db;
	public $message;
	public $code;

	public $COIN_NOT_ENOUGH_CODE = 500;
	public $SUCCESS_CODE = 200;

	public $COIN_NOT_ENOUGH = "tiket tidak cukup";
	public $SUCCESS = "success";

	function __construct($c){
		$this->c = $c;
		$this->db = $c->db;
	}

	public function get_prizes()
	{
		return $this->db->select('reedem_prizes',[
				'name',
				'description',
				'thumbnail_url',
				'price',
				'is_publish'
			],
			[
				'is_publish'=>1,
				'ORDER'=>[
					'price'=>"ASC"
				]
			]);
	}

	public function reedem($user_id,$prize_id,$alamat,$no_hp,$name)
	{
		//check user coin
		$userModel = $this->c->Auth;
		$usedCoin = 0;
		$userCoin = 0;


		$userTiket = $userModel->getTotalTiket($user_id);
		$usedTiket = $this->db->sum('reedem_prize_users',['[>]reedem_prizes(prize)'=>['prize_id'=>'id']],'prize.price',['reedem_prize_users.user_cred_id'=>$user_id]);
		$availableCoin = $userTiket-$usedTiket;

		$prizePrice = $this->getPrizePrice($prize_id);
		$retData = new ReturnData();

		if($availableCoin<$prizePrice)
		{
			$retData->set([],$this->COIN_NOT_ENOUGH_CODE,"false",$this->COIN_NOT_ENOUGH);
			return $retData;
		}else{
			$prize_name = $this->db->get('reedem_prizes','name',['id'=>$prize_id]);

			//create row reedem
			$this->db->insert('reedem_prize_users',[
				'user_cred_id'=>$user_id,
				'name'=>$name,
				'prize_id'=>$prize_id,
				'prize_name'=>$prize_name,
				'processed'=>0,
				'alamat_pengiriman'=>$alamat,
				'no_hp'=>$no_hp
			]);
			
			
			$retData->set([
				'tiket' => ($availableCoin-$pricePrice).''
			],$this->SUCCESS_CODE,"true",$this->SUCCESS);

			return $retData;
		}
	}

	public function getPrizePrice($prize_id)
	{
		return $this->db->get('reedem_prizes','price',['id'=>$prize_id]);
	}

	public function getHistoryReedem($user_id)
	{
		return $this->db->select('reedem_prize_users',[
			'name',
			'prize_name',
			'alamat_pengiriman',
			'no_hp',
			'processed'
		],['user_cred_id'=>$user_id]);
			
	}

	
}

?>