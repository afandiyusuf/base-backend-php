<?php

use Respect\Validation\Validator as V;
use \Helper\ReturnData as ReturnData;


class BarcodeController
{
	public $c;
	public $base_domain = "http://mahima.afandiyusuf.com/";

	public function __construct($container){
		$this->c = $container;
	}

	public function check($request, $response, $args){
		$barcode = $request->getParam("barcode");
		$barcodeModel = $this->c->Barcode;
		$retData = new ReturnData();
		$barcodeChecker = $barcodeModel->checkBarcode($barcode);

		if($barcodeChecker == Barcode::$NOT_FOUND)
		{
			$retData->set([],"406","false",'barcode tidak valid');
		}else if($barcodeChecker == Barcode::$CLAIMED)
		{
			$retData->set([],"407","false",'barcode sudah diclaim');
		}else{
			$retData->set([],"200","true",'barcode dikenali');
		}

		return $response->withJson($retData,200);
	}

	public function claim($request, $response, $args){
		$barcode = $request->getParam("barcode");
		$id = $request->getParam("id");

		$barcodeModel = $this->c->Barcode;
		$retData = new ReturnData();
		$barcodeChecker = $barcodeModel->claimBarcode($id,$barcode);

		if($barcodeChecker == Barcode::$NOT_FOUND)
		{
			$retData->set([],"406","false",'barcode tidak valid');
		}else if($barcodeChecker == Barcode::$CLAIMED)
		{
			$retData->set([],"407","false",'barcode sudah diclaim');
		}else{
			//get authModel
			$authModel = $this->c->Auth;
			//parsedJsonAdditional Data
			$parsedAddData = json_decode('[{"name":"point"}]');
			//get user coin
			$coin = $authModel->getAdditionalData($id,$parsedAddData);
			//add coin +1
			$total_coin = intval($coin[0]["val"]);
			$total_coin += 1;
			//prepare for update data
			$parsedAddData = json_decode('[{"name":"point","val":"'.$total_coin.'"}]');
			//update total coin
			$authModel->setAdditionalData($id,$parsedAddData);



			//set barcodes
			//parsedJsonAdditional Data
			$parsedAddData = json_decode('[{"name":"barcode_claimed"}]');
			//get user coin
			$barcode = $authModel->getAdditionalData($id,$parsedAddData);
			//add coin +1
			$total_barcode = intval($barcode[0]["val"]);
			$total_barcode += 1;
			//prepare for update data
			$parsedAddData = json_decode('[{"name":"barcode_claimed","val":"'.$total_barcode.'"}]');
			//update total coin
			$authModel->setAdditionalData($id,$parsedAddData);

			
			//prepare for return data
			$coin[0]["val"] = $total_coin."";
			$coin[1]["prop"] = "barcode_claimed";
			$coin[1]["val"] = $total_barcode."";
			//finish
			$retData->set($coin,"200","true",'Sukses, barcode berhasil diclaim');
		}

		return $response->withJson($retData,200);
	}

	public function exchange($request,$response,$args)
	{
		$id = $request->getParam("id");

		$authModel = $this->c->Auth;
		$retData = new ReturnData();

		//get user tiket
		$parsedAddData = json_decode('[{"name":"point"}]');
		//get user coin
		$point = $authModel->getAdditionalData($id,$parsedAddData);

		$total_point = intval($point[0]['val']);

		if($total_point > 0)
		{
			//minus one for playing slot
			$total_point -= 1;
			$total_tiket = 0;

			$barcodeModel = $this->c->Barcode;
			
			$total_probability = $barcodeModel->get_all_prob();
			$prob_tiket = rand(0,$total_probability);

			$all_list_slot = $barcodeModel->get_complete_list_slot();
				
			$initial_prob = 0;
			$total_list = count($all_list_slot);
			for ($i=0; $i < $total_list; $i++) {
				$initial_prob +=  intval($all_list_slot[$i]['probability']);
				if($prob_tiket <= $initial_prob)
				{
					$total_tiket = intval($all_list_slot[$i]['prize']);
					// you got a prize
					break;
				}
			}

			//get user tiket
			$parsedAddData = json_decode('[{"name":"tiket"}]');
			//get user coin
			$tiket = $authModel->getAdditionalData($id,$parsedAddData);

			$tiket_now = intval($tiket[0]['val']);
			$tiket_now += $total_tiket;
			

			$parsedAddData = json_decode('[{"name":"point","val":"'.$total_point.'"},{"name":"tiket","val":"'.$tiket_now.'"}]');
			
			//update total coin
			$authModel->setAdditionalData($id,$parsedAddData);
			
			//get user tiket
			$parsedAddData = json_decode('[{"name":"tiket"},{"name":"point"}]');
			//get user coin
			$datas = $authModel->getAdditionalData($id,$parsedAddData);

			
			$retData->set($datas,"20".$total_tiket,"true",'Selamat, kamu mendapatkan '.$all_list_slot[$i]['description']);

			$this->c->Barcode->logExcange($id,$total_tiket);
		}else{
			$retData->set([],"200","true",'Maaf koin kamu tidak cukup');
		}

		return $response->withJson($retData,200);

	}

	/**
	 * [POST/slot/list_exchange] api yang digunakan untuk mendapatkan list mesin slot.
	 * @param  $request, post variable -id  -access token
	 * @return json yang berisi tentang list mesin slot di apk
	 */
	public function list_slot($request,$response,$args)
	{
		$id = $request->getParam('id');

		$barcodeModel = $this->c->Barcode;
		$list_price = $barcodeModel->get_list_slot();
		$retData = new ReturnData();
		$retData->set(array('base_domain'=>$this->base_domain,'list_price'=>$list_price),"200","true","sukses");
		return $response->withJson($retData,200);
	}
}

?>