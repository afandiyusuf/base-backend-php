<?php

use Respect\Validation\Validator as V;
use \Helper\ReturnData as ReturnData;


class PrizeController
{
	public $c;
	public $base_domain = "http://mahima.afandiyusuf.com/";

	public function __construct($container){
		$this->c = $container;
	}

	public function get_prizes($request,$response,$args)
	{
		$retData = new ReturnData();

		$prizes = $this->c->Prize->get_prizes();

		$retData->set(["base_domain"=>$this->base_domain,"items"=>$prizes],"200","true",'success');	

		return $response->withJson($retData,200);
	}

	public function reedem_prize($request,$response,$args)
	{
		$retData = new ReturnData();
		$user_id = $request->getParam('id');
		$prize_id = $request->getParam('prize_id');
		$alamat = $request->getParam('alamat');
		$no_hp = $request->getParam('no_hp');
		$name = $request->getParam('name');

		$reedemResult = $this->c->Prize->reedem($user_id,$prize_id,$alamat,$no_hp,$name);

		return $response->withJson($reedemResult,200);
	}

	public function get_history($request,$response,$args)
	{
		$retData = new ReturnData();
		$user_id = $request->getParam('id');

		$prizeModel = $this->c->Prize;
		$history = $prizeModel->getHistoryReedem($user_id);
		$retData->set($history,"200","true","success");
		return $response->withJson($retData,200);
	}
}

?>