<?php

use Respect\Validation\Validator as V;
use \Helper\ReturnData as ReturnData;


class StoreController
{
	public $c;

	public function __construct($container){
		$this->c = $container;
	}

	public function getAll($request, $response, $args){
		$retData = new ReturnData();
		$Store = $this->c->Store;
		
		$allDatas = $Store->get_all_provinces();
		
		for($i=0;$i<count($allDatas);$i++)
		{
			$tempStore = $Store->get_store_by_province($allDatas[$i]["id"]);
			$allDatas[$i]["total"] = "".count($tempStore);
			$allDatas[$i]["stores"] = $tempStore;

		}

		$retData->set(
			$allDatas
			//"all_store"=>$Store->get_all_store()
		,"200","true","get store sukses");
		return $response->withJson($retData,200);
	}
}