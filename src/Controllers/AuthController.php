<?php

use Respect\Validation\Validator as V;
use \Helper\ReturnData as ReturnData;
class AuthController
{
	public $c;
	public $base_domain = "http://mahima.afandiyusuf.com/";

	public function __construct($container){
		$this->c = $container;
	}
	
	/**
	 * api untuk login user
	 * @return json with access token
	 */
	public function login($request, $response, $args) {

		$retData = new ReturnData();

		// This will return the validator instance
		$validator = $this->c->validator->validate($request, [
		    'username' => V::length(1, 20)->alnum('_')->noWhitespace(),
		    'password' => V::length(1, 100)->alnum('_')
		]);

		if($validator->isValid())
		{
			//validation success
			$authModel = $this->c->Auth;
			$username 	= $request->getParam('username');
			$password 	= $request->getParam('password');


			$loginChecker = $authModel->loginWithUsername($username,$password);

			if($loginChecker == Auth::$ERROR)
			{
				$retData->set([],"401","false",'username or password invalid');
			}else{

				$parsedAddData = json_decode('[{"name":"full_name"},{"name":"phone"}]');
				
				//login success
				//TODO: Make login logic
				$user_id = $loginChecker;
				$accessToken = $authModel->generateAccessToken($user_id);

				$datas  = $authModel->getAdditionalData($user_id,$parsedAddData);
				$retData->set([
					"id" => $user_id."",
					"access_token"=>$accessToken,
					"additionalData"=>$datas,
					"login_image"=>$authModel->getLoginScreenUrl()["thumbnail_url"],
					"base_domain"=>$this->base_domain
				],"200","true",'success');	
			}

		}else{
			//validation error
			$retData->set(["errors"=>$validator->getErrors()],"600","false",'validation_error');
		}

		return $response->withJson($retData,200);


	}

	/**
	 * api untuk register user, hanya digunakan untuk apps apk
	 * @return json status register, dan access token jika register berhasil
	 */
	public function register($request, $response, $args){
		$retData = new ReturnData();

		// This will return the validator instance
		$validator = $this->c->validator->validate($request, [
		    'username' => V::length(1, 20)->alnum('_')->noWhitespace(),
		    'password' => V::length(1, 100)->alnum('_'),
		    'email' => V::email(),
		    'additional_data' =>V::json()
		]);

		if($validator->isValid())
		{
			//validation success
			$authModel = $this->c->Auth;
			$username 	= $request->getParam("username");
			$password 	= $request->getParam('password');
			$email = $request->getParam('email');
			$additional_data = $request->getParam('additional_data');
			$parsedAddData = json_decode($additional_data);

			$addDataValidator = $authModel->validateAdditionalData($parsedAddData);

			//check if additional data is invalid
			if($addDataValidator == Auth::$ERROR)
			{
				//additional data invalid
				$retData->set([],"600","false",'additional data invalid');
			}else{
				//check if username or email is available
				$availableChecker = $authModel->checkUsernameEmail($username,$email);
				if($availableChecker == Auth::$SUCCESS)
				{
					//login success
					//TODO: Make login logic
					$registeredUserId = $authModel->register($username,$email,$password);
					$authModel->setAdditionalData($registeredUserId, $parsedAddData);
					$accessToken = $authModel->generateAccessToken($registeredUserId);

					$retData->set([
							"id" => $registeredUserId,
							"username" => $username,
							"email"=>$email,
							"additionalData"=>$parsedAddData,
							"access_token"=>$accessToken,
						],"200","true",'success');
				}else{
					$retData->set([],"401","false",$availableChecker);
				}
			}
		}else{
			//validation error
			$retData->set(["errors"=>$validator->getErrors()],"600","false",'validation_error');
		}

		return $response->withJson($retData,200);
	}

	public function get_meta($request, $response, $args){
		$retData = new ReturnData();
		//validation success
		$authModel 		= $this->c->Auth;
		$accessToken 	= $request->getParam('access_token');
		$id 			= $request->getParam('id');

		
		$additional_data = $request->getParam('additional_data');
		$parsedAddData = json_decode($additional_data);

		// $addDataValidator = $authModel->validateAdditionalData($parsedAddData);
		// if($addDataValidator == Auth::$ERROR)
		// {
		// 	$retData->set([],"401","false",'property tidak valid');
		// }else{
			$additional_user_data = $authModel->getAdditionalData($id,$parsedAddData);
			$retData->set($additional_user_data,"200","true",'sukses');
		//}
		

		return $response->withJson($retData,200);
	}

	public function update_meta($request, $response, $args)
	{
		$retData = new ReturnData();
		//validation success
		$authModel 		= $this->c->Auth;
		$accessToken 	= $request->getParam('access_token');
		$id 			= $request->getParam('id');

		$additional_data = $request->getParam('additional_data');
		$parsedAddData = json_decode($additional_data);
		$addDataValidator = $authModel->validateAdditionalData($parsedAddData);
		if($addDataValidator == Auth::$ERROR)
		{
			$retData->set([],"401","false",'property tidak valid');
		}else{
			$additional_data = $request->getParam('additional_data');
			$parsedAddData = json_decode($additional_data);
			$authModel->setAdditionalData($id,$parsedAddData);
			$datas  = $authModel->getAdditionalData($id,$parsedAddData);
			$retData->set($datas,"200","true",'sukses');
		}
		
	

		return $response->withJson($retData,200);
	}

	public function updateUserData($request,$response,$args)
	{

		
		$id = $request->getParam("id");

		$this->update_meta($request,$response,$args);
		$authModel 		= $this->c->Auth;
		if($request->getParam('password') != ""){
			$password = $request->getParam('password');
			$authModel->updatePassword($id,$password);
		}
		$retData = new ReturnData();
		$additional_data = $request->getParam('additional_data');
		$parsedAddData = json_decode($additional_data);
		$datas  = $authModel->getAdditionalData($id,$parsedAddData);
		$retData->set($datas,"200","true",'sukses');
		return $response->withJson($retData,200);
	}

	public function getBest10($request,$response,$args)
	{
		$barcodeModel = $this->c->Barcode;
		$data = $barcodeModel->getBest10();
		//var_dump($data);
		$retData = new ReturnData();
		$retData->set($data,"200","true","sukses");

		return $response->withJson($retData,200);
	}

	public function get_prizes($request,$response,$args)
	{
		
	}
}

?>