<?php

namespace Helper;

class ReturnData{
	
	public $code;
	public $status;
	public $message;
	public $data = [];
	
	
	public function set($data,$code,$status,$message){
		$this->data = $data;
		$this->status = $status;
		$this->code = $code;
		$this->message = $message;
	}
}




?>