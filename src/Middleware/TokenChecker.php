
<?php
use \Helper\ReturnData;
class TokenChecker
{
    private $c;
    public function __construct($c)
    {
        $this->c = $c;
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        $container = $this->c->getContainer();
        $authModel      = $container["Auth"];
        $accessToken    = $request->getParam('access_token');
        $id             = $request->getParam('id');

        $accessTokenValidator = $authModel->validateAccessToken($id,$accessToken);
        if($accessTokenValidator == "auth_success")
        {
            $response = $next($request, $response);
        }else{
            $retData = new ReturnData();
            $retData->set([],401,false,'akses token tidak valid');
            return $response->withJson($retData,200);
        }

            return $response;
    }
}

?>