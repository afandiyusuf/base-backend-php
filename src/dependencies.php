<?php
// DIC configuration
use Medoo\Medoo;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

$container['db'] = function ($c) {
    $settings = $c->get('settings')['medoo'];
    return new Medoo([
        'database_type' => $settings['database_type'],
        'database_name' => $settings['database_name'],
        'server' => $settings['server'],
        'username' => $settings['username'],
        'password' => $settings['password']
    ]); 
};


// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['validator'] = function($c){
    return new Awurth\SlimValidation\Validator();
};

//AuthController
$container['AuthController'] = function($c){
    return new AuthController($c);
};
//AuthModel
$container['Auth'] = function($c){
	return new Auth($c);
};



//BarcodeController
$container['BarcodeController'] = function($c){
    return new BarcodeController($c);
};

//BarcodeModel
$container['Barcode'] = function($c){
    return new Barcode($c);
};


//BarcodeController
$container['PrizeController'] = function($c){
    return new PrizeController($c);
};

//BarcodeModel
$container['Prize'] = function($c){
    return new Prize($c);
};

$container['Store'] = function($c){
    return new Store($c);
};

$container['StoreController'] = function($c)
{
    return new StoreController($c);
};
