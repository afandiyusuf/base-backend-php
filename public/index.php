<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

 require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

require __DIR__.'/../src/Models/Store.php';
require __DIR__.'/../src/Controllers/StoreController.php';

// Middleware
require __DIR__.'/../src/Middleware/TokenChecker.php';

//Set up Controller
require __DIR__.'/../src/Controllers/AuthController.php';
//Set up Barcode Controller
require __DIR__.'/../src/Controllers/BarcodeController.php';

//Set up Barcode
require __DIR__. '/../src/Models/Auth.php';
require __DIR__.'/../src/Models/Barcode.php';

//Set up Prize
require __DIR__. '/../src/Models/Prize.php';
require __DIR__.'/../src/Controllers/PrizeController.php';


//Set up Helper
require __DIR__.'/../src/Helpers/ReturnData.php';



// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';




// Run app
$app->run();
